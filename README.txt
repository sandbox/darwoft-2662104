About
=====
Integrates the RhinoSlider library into Drupal.



About RhinoSlider
----------------

Library available at http://www.rhinoslider.com/

Rhinoslider is the most flexible jQuery slideshow there is. 
Be welcome to try it yourself. Not only do we offer a variety of effects, 
we also allow you to add your own styles, effects and features to the slider.
- Dual licensed under the MIT or GPL Version 2 licenses

Installation
============

Dependencies
------------

- [Libraries API 2.x](http://drupal.org/project/libraries)
- [RhinoSlider Library](http://www.rhinoslider.com/)

Tasks
-----

1. Download the RhinoSlider library from 
   http://www.rhinoslider.com/demo-download/
2. Unzip the file and rename the folder to "rhinoslider" (do not user capital 
characters)
3. Put the folder in a libraries directory
    - Ex: sites/all/libraries
4. The following files are required
    - js/rhinoslider-1.05.min.js
    - css/rhinoslider-1.05.css
5. Ensure you have a valid path similar to this one for all files
    - Ex: sites/all/libraries/rhinolider/js/rhinoslider-1.05.min.js
